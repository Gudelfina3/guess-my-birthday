
from random import randint

    # our greeting. Get user name

user_name = input("Hi! What is your name? ")

for number_of_guesses in range(3):

    # generate the range to guess from
    birthday_month = (randint(1,12))
    birthday_year = (randint(1924, 2004))

    our_guess = (f"{birthday_month}/{birthday_year}")

    # ask user if our guess is right. This is the first guess

    ask_user = print(f"{user_name} were you born in {our_guess}?")

    user_answer = input("yes or no?").lower()

    if user_answer == "yes":
        print("I knew it!")
        break
    # ask user again if it's wrong. Repeat until right answer is
    #reached or all guesses are used up
    elif number_of_guesses == 2:
        print("I have better things to do. Goodbye")
    else:
        print("Let me try again")
